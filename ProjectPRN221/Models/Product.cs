﻿using ProjectPRN221.Models;
using System;
using System.Collections.Generic;

namespace ProjectPRN221.Models;

public partial class Product
{
    public int ProductId { get; set; }

    public string? ProductName { get; set; }

    public int SupplierId { get; set; }

    public int CategoryId { get; set; }

    public string QuantityPerUnit { get; set; } = null!;

    public decimal UnitPrice { get; set; }

    public string? ProductImage { get; set; }

    public DateTime Date { get; set; }

    public string? CreateBy { get; set; }

    public virtual ICollection<BillDetail> BillDetails { get; set; } = new List<BillDetail>();

    public virtual Category Category { get; set; } = null!;

    public virtual Supplier Supplier { get; set; } = null!;
}
