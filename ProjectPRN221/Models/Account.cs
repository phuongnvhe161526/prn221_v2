﻿using ProjectPRN221.Models;
using System;
using System.Collections.Generic;

namespace ProjectPRN221.Models;

public partial class Account
{
    public int AccountId { get; set; }

    public string Username { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string? FullName { get; set; }

    public int? Role { get; set; }

    public virtual ICollection<Bill> Bills { get; set; } = new List<Bill>();
}
