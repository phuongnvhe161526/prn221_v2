﻿using ProjectPRN221.Models;
using System;
using System.Collections.Generic;

namespace ProjectPRN221.Models;

public partial class Bill
{
    public int BillId { get; set; }

    public int AccountId { get; set; }

    public DateTime BillDate { get; set; }

    public virtual Account Account { get; set; } = null!;

    public virtual ICollection<BillDetail> BillDetails { get; set; } = new List<BillDetail>();

}
