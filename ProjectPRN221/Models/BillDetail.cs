﻿using ProjectPRN221.Models;
using System;
using System.Collections.Generic;

namespace ProjectPRN221.Models;

public partial class BillDetail
{
    public int BillId { get; set; }

    public int ProductId { get; set; }

    public decimal UnitPrice { get; set; }

    public int Quantity { get; set; }

    public virtual Bill Bill { get; set; } = null!;

    public virtual Product Product { get; set; } = null!;
}
