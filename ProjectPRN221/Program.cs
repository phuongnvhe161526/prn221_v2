﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ProjectPRN221.Models;

namespace ProjectPRN221
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddDbContext<ProjectPrn221Context>(options =>
                 options.UseSqlServer(builder.Configuration.GetConnectionString("MyContr")));
            builder.Services.AddDistributedMemoryCache(); // Thêm dịch vụ bộ nhớ đệm (cache) cho phiên
            builder.Services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(20); // Thiết lập thời gian chờ phiên hết hiệu lực
            });
            builder.Services.AddHttpContextAccessor();
            var app = builder.Build();


            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapRazorPages();
            app.UseSession();

            app.Run();
        }
    }
}