using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;
using System.Diagnostics;

namespace ProjectPRN221.Pages.Account_View
{
    public class ProfileModel : PageModel
    {
        private readonly ProjectPrn221Context _context;

        private readonly IHttpContextAccessor _httpContextAccessor;


        public ProfileModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public Account Account { get; set; } = default!;



        public async Task<IActionResult> OnGetAsync(int? id)
        {

            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Account", out var AccountData))
            {
                Account = System.Text.Json.JsonSerializer.Deserialize<Account>(AccountData);
                return Page();
            }

            var account = await _context.Accounts.FirstOrDefaultAsync(m => m.AccountId == id);
            if (account == null)
            {
                return NotFound();
            }
            else
            {
                Account = account;
            }

            return Page();
        }
    }
}
