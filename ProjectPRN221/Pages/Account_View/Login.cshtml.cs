﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Account_View
{
    public class LoginModel : PageModel
    {
        private readonly ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public LoginModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Account Account { get; set; } = default!;

        [HttpPost]
        public async Task<IActionResult> OnPostAsync()
        {
            Account = _context.Accounts.FirstOrDefault(ac => ac.Username.Equals(Account.Username) && ac.Password.Equals(Account.Password));
            if ( Account == null)
            {
                ModelState.AddModelError("Account.Password", "Mật khẩu hoặc tên đăng nhập không khớp.");
                return Page();
            }
            var serializedCart = System.Text.Json.JsonSerializer.Serialize(Account);
            _httpContextAccessor.HttpContext.Session.Set("Account", System.Text.Encoding.UTF8.GetBytes(serializedCart));

            return RedirectToPage("/Home/Home");
        }

    }
}
