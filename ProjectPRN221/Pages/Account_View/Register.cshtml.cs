﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Account_View
{
    public class RegisterModel : PageModel
    {
        private readonly ProjectPrn221Context _context;

        public RegisterModel(ProjectPrn221Context context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Account Account { get; set; } = default!;
        [BindProperty]
        public string RePassword { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            Account.Role = 0;
            if (!ModelState.IsValid || _context.Accounts == null || Account == null)
            {

                return Page();
            }
            if (Account.Password != RePassword)
            {
                ModelState.AddModelError("RePassword", "Mật khẩu xác nhận không khớp.");
                return Page();
            }

            _context.Accounts.Add(Account);

            await _context.SaveChangesAsync();
            return RedirectToPage("./Login");

        }
    }
}

