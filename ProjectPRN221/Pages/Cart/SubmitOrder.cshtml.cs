﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Cart
{
    public class SubmitOrderModel : PageModel
    {
        private readonly ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SubmitOrderModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public IActionResult OnGet()
        {
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            Account account = null;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Account", out var Data))
            {
                account = System.Text.Json.JsonSerializer.Deserialize<Account>(Data);
            }
            List<Cart> cartItems;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Cart", out var cartData))
            {
                cartItems = System.Text.Json.JsonSerializer.Deserialize<List<Cart>>(cartData);
            }
            else
            {
                cartItems = new List<Cart>();
            }
            if (cartItems.Count > 0)
            {
                DateTime RequiredDate = DateTime.Parse(HttpContext.Request.Form["RequiredDate"]);

                Bill bill = new Bill();
                bill.AccountId = account.AccountId;
                bill.BillDate = RequiredDate;


                _context.Bills.Add(bill);

                bill = _context.Bills.SingleOrDefault(item => item.BillId == bill.BillId);

                foreach (var item in cartItems)
                {
                    bill.BillDetails.Add(new BillDetail()
                    {
                        ProductId = item.Product.ProductId,
                        UnitPrice = item.Product.UnitPrice,
                        Quantity = item.num
                    });
                }
                await _context.SaveChangesAsync();
            }
            else
            {
                return Page();
            }
            return Page();
        }
    }
}
