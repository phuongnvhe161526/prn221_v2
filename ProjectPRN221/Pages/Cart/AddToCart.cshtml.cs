﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;
using ProjectPRN221.Pages.Cart;

namespace ProjectPRN221.Pages.Cart
{
    public class AddToCartModel : PageModel
    {
        private readonly    ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AddToCartModel(  ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<IActionResult> OnGetAsync(int? id, int? num)

        {
            int? pdid = id;
            Product product = _context.Products.SingleOrDefault(item => item.ProductId == pdid);

            List<Cart> cartItems;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Cart", out var cartData))
            {
                cartItems = System.Text.Json.JsonSerializer.Deserialize<List<Cart>>(cartData);
            }
            else
            {
                cartItems = new List<Cart>();
            }

            var existingCartItem = cartItems.SingleOrDefault(item => item.Product.ProductId == pdid);
            if (existingCartItem != null)
            {
                if (num <= existingCartItem.num)
                {
                    existingCartItem.num -= 1;
                }
                else
                {
                    existingCartItem.num += 1;
                }
                if (existingCartItem.num == 0)
                {
                    cartItems.Remove(existingCartItem);
                }

            }
            else
            {
                var card = new Cart
                {
                    Product = product,
                    num = 1
                };
                cartItems.Add(card);
            }


            // L?u danh sách s?n ph?m vào session
            var serializedCart = System.Text.Json.JsonSerializer.Serialize(cartItems);
            _httpContextAccessor.HttpContext.Session.Set("Cart", System.Text.Encoding.UTF8.GetBytes(serializedCart));

            string refererUrl = Request.Headers["Referer"].ToString();
            return Redirect(refererUrl);
        }
    }
}
