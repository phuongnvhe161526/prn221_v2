﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Cart
{
    public class CartModel : PageModel
    {
        private readonly ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CartModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }
        public IList<Cart> Cart { get; set; } = default!;

        public async Task OnGetAsync()
        {
            List<Cart> cartItems;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Cart", out var cartData))
            {
                cartItems = System.Text.Json.JsonSerializer.Deserialize<List<Cart>>(cartData);
            }
            else
            {
                cartItems = new List<Cart>();
            }
            Cart = cartItems;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            Account account = new Account();
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Account", out var Data))
            {
                account = System.Text.Json.JsonSerializer.Deserialize<Account>(Data);
            }

            List<Cart> cartItems;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Cart", out var cartData))
            {
                cartItems = System.Text.Json.JsonSerializer.Deserialize<List<Cart>>(cartData);
            }
            else
            {
                cartItems = new List<Cart>();
            }

            if (cartItems.Count > 0)
            {
                DateTime RequiredDate = DateTime.Parse(HttpContext.Request.Form["RequiredDate"]);

                Bill bill = new Bill();
                bill.BillDate = RequiredDate;
                bill.AccountId = account.AccountId;

                _context.Bills.Add(bill);

                foreach (var item in cartItems)
                {
                    var product = await _context.Products.FindAsync(item.Product.ProductId);
                    if (product != null && int.TryParse(product.QuantityPerUnit, out int quantity) && quantity >= item.num)
                    {
                        quantity -= item.num;
                        product.QuantityPerUnit = quantity.ToString();
                        bill.BillDetails.Add(new BillDetail()
                        {
                            ProductId = item.Product.ProductId,
                            UnitPrice = item.Product.UnitPrice,
                            Quantity = item.num
                        });
                    }
                    else
                    {
                        return Page();
                    }
                }


                await _context.SaveChangesAsync();
            }

            // Clear the cart after successful creation of the bill
            _httpContextAccessor.HttpContext.Session.Remove("Cart");

            TempData["SuccessMessage"] = "Đặt Hàng Thành Công. Theo Dõi Đơn Hàng Trong tab OrderManager.";
            return Page();
        }




    }
    public class Cart
    {
        public Product Product { get; set; }
        public int num { get; set; }
    }
}