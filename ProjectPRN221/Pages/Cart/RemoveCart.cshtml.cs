﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221.Models;
using ProjectPRN221.Pages.Cart;

namespace ProjectPRN221.Pages.Cart
{
    public class RemoveCartModel : PageModel
    {
        private readonly ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RemoveCartModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<IActionResult> OnGetAsync(int? id)

        {
            int? pdid = id;
            Product product = _context.Products.SingleOrDefault(item => item.ProductId == pdid);
            List<Cart> cartItems;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Cart", out var cartData))
            {
                cartItems = System.Text.Json.JsonSerializer.Deserialize<List<Cart>>(cartData);
            }
            else
            {
                cartItems = new List<Cart>();
            }

            var existingCartItem = cartItems.SingleOrDefault(item => item.Product.ProductId == pdid);
            if (existingCartItem != null)
            {
                cartItems.Remove(existingCartItem);
            }

            var serializedCart = System.Text.Json.JsonSerializer.Serialize(cartItems);          
            _httpContextAccessor.HttpContext.Session.Set("Cart", System.Text.Encoding.UTF8.GetBytes(serializedCart));
            string refererUrl = Request.Headers["Referer"].ToString();

            return Redirect(refererUrl);
        }
    }
}
