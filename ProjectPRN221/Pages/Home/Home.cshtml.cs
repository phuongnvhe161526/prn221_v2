﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPRN221.Pages.Home
{
    public class HomeModel : PageModel
    {
        private readonly ProjectPrn221Context _context;

        public HomeModel(ProjectPrn221Context context)
        {
            _context = context;
        }

        public IList<Product> Product { get; set; } = default!;
        public List<Category> Categories { get; set; } = new List<Category>();

        [BindProperty(SupportsGet = true)]
        public string SearchTerm { get; set; }

        public async Task OnGetAsync(int categoryId)
        {
            Categories = _context.Categories.ToList();

            IQueryable<Product> productsQuery = _context.Products
                .Include(p => p.Category)
                .Include(p => p.Supplier);

            if (!string.IsNullOrEmpty(SearchTerm))
            {
                productsQuery = productsQuery.Where(p => p.ProductName.Contains(SearchTerm));
            }

            if (categoryId != 0)
            {
                productsQuery = productsQuery.Where(p => p.CategoryId == categoryId);
            }

            Product = await productsQuery.ToListAsync();

            foreach (var product in Product)
            {
                var imagePath = Path.Combine("~/Images", product.ProductImage);
                product.ProductImage = imagePath;
            }
        }
    }
}
