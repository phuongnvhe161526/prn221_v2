﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Product_View
{
    public class DetailsModel : PageModel
    {
        private readonly ProjectPRN221.Models.ProjectPrn221Context _context;

        public DetailsModel(ProjectPRN221.Models.ProjectPrn221Context context)
        {
            _context = context;
        }

      public Product Product { get; set; } = default!; 
      public Supplier Supplier { get; set; }  
      public Category Category { get; set; }


        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products.Include(s => s.Supplier).Include(c => c.Category)
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }
            else 
            {
                Product = product;

                var imagePath = Path.Combine("~/Images", product.ProductImage);
                product.ProductImage = imagePath;
            }
            return Page();
        }
    }
}
