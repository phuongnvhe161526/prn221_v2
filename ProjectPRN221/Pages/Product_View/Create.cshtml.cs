﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Product_View
{
    public class CreateModel : PageModel
    {
        private readonly ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CreateModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor= httpContextAccessor;
        }


        public async Task OnGetAsync()
        {
           
            Categories = await _context.Categories.ToListAsync();
                Suppliers = await _context.Suppliers.ToListAsync();
        }
        

        [BindProperty]
        public Product Product { get; set; } = default!;
        public IList<Category> Categories { get; set; }
        public IList<Supplier> Suppliers { get; set; }

        [BindProperty]
        public IFormFile ImageFile { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            Account account = null;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Account", out var AccountData))
            {
                account = System.Text.Json.JsonSerializer.Deserialize<Account>(AccountData);
            }
            if (_context.Products == null || Product == null)
            {
                return Page();
            }
            if (ImageFile != null && ImageFile.Length > 0)
            {


                //  lưu ảnh vào thư mục Images trên máy chủ
                var imagePath = Path.Combine("wwwroot/Images", ImageFile.FileName);
                using (var stream = new FileStream(imagePath, FileMode.Create))
                {
                    await ImageFile.CopyToAsync(stream);
                }

                var imageUrl = "" + ImageFile.FileName;
                // Lưu imageUrl vào cơ sở dữ liệu
                Product.ProductImage = imageUrl;
            }
            else
            {
                ModelState.AddModelError("imageFile", "Vui lòng chọn một tệp hình ảnh.");
                return Page();
            }
        
            Product.CreateBy = account.FullName;
            _context.Products.Add(Product);
            await _context.SaveChangesAsync();
            //PopulateDepartmentsDropDownList(_context, Course.DepartmentId);

            return RedirectToPage("./Index");
        }
    }
}
