﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Product_View
{
    public class DeleteModel : PageModel
    {
        private readonly ProjectPRN221.Models.ProjectPrn221Context _context;

        public DeleteModel(ProjectPRN221.Models.ProjectPrn221Context context)
        {
            _context = context;
        }

        [BindProperty]
      public Product Product { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products.Include(s => s.Supplier).Include(c => c.Category)
                .FirstOrDefaultAsync(m => m.ProductId == id);

            if (product == null)
            {
                return NotFound();
            }
            else 
            {
                Product = product;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .Include(p => p.BillDetails)
                .ThenInclude(bd => bd.Bill)
                .FirstOrDefaultAsync(m => m.ProductId == id);

            if (product == null)
            {
                return NotFound();
            }

            // Remove related BillDetails
            _context.BillDetails.RemoveRange(product.BillDetails);

            // Remove related Bills
            var billIds = product.BillDetails.Select(bd => bd.BillId).ToList();
            var billsToRemove = await _context.Bills.Where(b => billIds.Contains(b.BillId)).ToListAsync();
            _context.Bills.RemoveRange(billsToRemove);

            // Remove the product
            _context.Products.Remove(product);

            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }

    }
}
