﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using Microsoft.DotNet.Scaffolding.Shared.Messaging;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Product_View
{
    public class EditModel : PageModel
    {
        private readonly ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EditModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        [BindProperty]
        public Product Product { get; set; } = default!;
        public IList<Category> Categories { get; set; }
        public IList<Supplier> Suppliers { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products.Include(s=>s.Supplier)
                .Include(s=>s.Category)
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }
            Product = product;
            if (_context.Products != null)
            {
                Categories = await _context.Categories.ToListAsync();
                Suppliers = await _context.Suppliers.ToListAsync();

            }

            return Page();
        }
        [BindProperty]
        public IFormFile imageFile { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            Account account = null;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Account", out var AccountData))
            {
                account = System.Text.Json.JsonSerializer.Deserialize<Account>(AccountData);
            }

            Product.CreateBy = account.FullName;
            _context.Attach(Product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(Product.ProductId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ProductExists(int id)
        {
            return (_context.Products?.Any(e => e.ProductId == id)).GetValueOrDefault();
        }
    }
}
