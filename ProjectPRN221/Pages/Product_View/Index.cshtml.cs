﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Product_View
{
    public class IndexModel : PageModel
    {
        private readonly ProjectPrn221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IndexModel(ProjectPrn221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public IList<Product> Product { get;set; } = default!;

        public async Task OnGetAsync()
        {
            Account account = null;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("Account", out var AccountData))
            {
                account = System.Text.Json.JsonSerializer.Deserialize<Account>(AccountData);
            }
            if (_context.Products != null)
            {
                Product = await _context.Products
                .Include(p => p.Category)
                .Include(p => p.Supplier).ToListAsync();

                foreach (var product in Product)
                {
                    var imagePath = Path.Combine("~/Images", product.ProductImage);
                    product.ProductImage = imagePath;
                }

            }
        }
    }
}
