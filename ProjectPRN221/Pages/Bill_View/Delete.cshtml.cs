﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Bill_View
{
    public class DeleteModel : PageModel
    {
        private readonly ProjectPRN221.Models.ProjectPrn221Context _context;

        public DeleteModel(ProjectPRN221.Models.ProjectPrn221Context context)
        {
            _context = context;
        }

        [BindProperty]
      public Bill Bill { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.Bills == null)
            {
                return NotFound();
            }

            var bill = await _context.Bills.Include(a => a.Account).FirstOrDefaultAsync(m => m.BillId == id);

            if (bill == null)
            {
                return NotFound();
            }
            else 
            {
                Bill = bill;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.Bills == null)
            {
                return NotFound();
            }

            var bill = await _context.Bills
                .Include(b => b.BillDetails) // Include related BillDetails
                .FirstOrDefaultAsync(m => m.BillId == id);

            if (bill != null)
            {
                // Remove related BillDetails
                _context.BillDetails.RemoveRange(bill.BillDetails);

                // Remove the Bill itself
                _context.Bills.Remove(bill);

                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }

    }
}
