﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Bill_View
{
    public class EditModel : PageModel
    {
        private readonly ProjectPRN221.Models.ProjectPrn221Context _context;

        public EditModel(ProjectPRN221.Models.ProjectPrn221Context context)
        {
            _context = context;
        }

        [BindProperty]
        public Bill Bill { get; set; } = default!;
        public IList<Account> Accounts { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            
            var bill =  await _context.Bills.FirstOrDefaultAsync(m => m.BillId == id);
            if (bill == null)
            {
                return NotFound();
            }
            Bill = bill;
            if (_context.Bills != null)
            {
                Accounts = await _context.Accounts.ToListAsync();
            }

            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            _context.Attach(Bill).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BillExists(Bill.BillId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool BillExists(int id)
        {
          return (_context.Bills?.Any(e => e.BillId == id)).GetValueOrDefault();
        }
    }
}
