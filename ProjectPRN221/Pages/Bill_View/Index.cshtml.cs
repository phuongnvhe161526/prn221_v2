﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.Bill_View
{
    public class IndexModel : PageModel
    {
        private readonly ProjectPrn221Context _context;

        public IndexModel(ProjectPrn221Context context)
        {
            _context = context;
        }

        public IList<Bill> Bill { get; set; } = default!;
        public Dictionary<int, decimal> TotalAmounts { get; set; } = new Dictionary<int, decimal>();

        public async Task OnGetAsync()
        {
            if (_context.Bills != null)
            {
                Bill = await _context.Bills
                    .Include(b => b.Account)
                     .Include(b => b.BillDetails)
                    .ToListAsync();

                foreach (var bill in Bill)
                {
                    decimal totalAmount = 0;

                    foreach (var billDetail in bill.BillDetails)
                    {
                        totalAmount += billDetail.UnitPrice * billDetail.Quantity;
                    }

                    TotalAmounts.Add(bill.BillId, totalAmount);
                }
            }
        }
    }
}
