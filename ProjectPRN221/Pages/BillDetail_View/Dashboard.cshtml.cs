﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.BillDetail_View
{
    public class DashboardModel : PageModel
    {
        private readonly ProjectPrn221Context _context;

        public DashboardModel(ProjectPrn221Context context)
        {
            _context = context;
        }

        public IList<BillDetail> BillDetail { get; set; } = default!;
        public decimal GrandTotal { get; set; }
        public int TotalOrder { get; set; }
        public int ProductOrder { get; set; }

        public string BestProduct { get; set; }
        public int ProductLowestOrder { get; set; }

        public string LowestProduct { get; set; }
        public async Task OnGetAsync(string sortCriteria)
        {
            if (_context.BillDetails != null)
            {
                BillDetail = await _context.BillDetails
                    .Include(b => b.Bill)
                    .Include(b => b.Product)
                    .ToListAsync();

                GrandTotal = BillDetail.Sum(item => item.UnitPrice * item.Quantity);
                TotalOrder = BillDetail.Sum(item => item.Quantity);

                // Lấy ra sản phẩm được order nhiều nhất
                var mostOrderedProduct = BillDetail
                    .GroupBy(item => item.ProductId)
                    .OrderByDescending(group => group.Sum(item => item.Quantity))
                    .FirstOrDefault();

                if (mostOrderedProduct != null)
                {
                    // mostOrderedProduct.Key sẽ chứa ProductId của sản phẩm được order nhiều nhất
                    // mostOrderedProduct.Sum(item => item.Quantity) sẽ chứa tổng số lượng đặt hàng của sản phẩm đó
                    int mostOrderedProductId = mostOrderedProduct.Key;
                    int mostOrderedQuantity = mostOrderedProduct.Sum(item => item.Quantity);

                    // Lấy thông tin sản phẩm từ ProductId
                    Product mostOrderedProductInfo = await _context.Products.FindAsync(mostOrderedProductId);
                    BestProduct = mostOrderedProductInfo.ProductName;
                    ProductOrder = mostOrderedQuantity;
                }

                // Lấy ra sản phẩm được order ít nhất
                var leastOrderedProduct = BillDetail
                    .GroupBy(item => item.ProductId)
                    .OrderBy(group => group.Sum(item => item.Quantity))
                    .FirstOrDefault();

                if (leastOrderedProduct != null)
                {
                    // leastOrderedProduct.Key sẽ chứa ProductId của sản phẩm được order ít nhất
                    // leastOrderedProduct.Sum(item => item.Quantity) sẽ chứa tổng số lượng đặt hàng của sản phẩm đó
                    int leastOrderedProductId = leastOrderedProduct.Key;
                    int leastOrderedQuantity = leastOrderedProduct.Sum(item => item.Quantity);

                    // Lấy thông tin sản phẩm từ ProductId
                    Product leastOrderedProductInfo = await _context.Products.FindAsync(leastOrderedProductId);
                    LowestProduct = leastOrderedProductInfo.ProductName;
                    ProductLowestOrder = leastOrderedQuantity;
                }
                if (!string.IsNullOrEmpty(sortCriteria))
                {
                    switch (sortCriteria)
                    {
                        case "BestProduct":
                            BillDetail = BillDetail
                 .GroupBy(item => item.Product.ProductName)
                 .OrderByDescending(group => group.Sum(item => item.Quantity))
                 .SelectMany(group => group)
                 .ToList();
                            break;
                        case "LowestProduct":
                            BillDetail = BillDetail
                .GroupBy(item => item.Product.ProductName)
                .OrderBy(group => group.Sum(item => item.Quantity))
                .SelectMany(group => group)
                .ToList();
                            break;

                        default:
                            break;
                    }
                }
                else
                {

                    BillDetail = await _context.BillDetails
                        .Include(b => b.Bill)
                        .Include(b => b.Product)
                        .ToListAsync();
                }
            }
        }
        }
    }

