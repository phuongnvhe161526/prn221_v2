﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectPRN221.Pages.BillDetail_View
{
    public class IndexModel : PageModel
    {
        private readonly ProjectPRN221.Models.ProjectPrn221Context _context;

        public IndexModel(ProjectPRN221.Models.ProjectPrn221Context context)
        {
            _context = context;
        }

        [BindProperty(SupportsGet = true)]
        public int? BillId { get; set; }

        public IList<BillDetail> BillDetail { get; set; } = default!;


        public async Task OnGetAsync(int billId)
        {
            if (billId != null)
            {


                BillDetail = await _context.BillDetails
                    .Include(b => b.Bill)
                    .Include(b => b.Product)
                    .Where(b => b.BillId == billId)
                    .ToListAsync();

                foreach (var billDetail in BillDetail)
                {
                    var imagePath = Path.Combine("~/Images", billDetail.Product.ProductImage);
                    billDetail.Product.ProductImage = imagePath;
                }

            }
           

        }
    }
}
