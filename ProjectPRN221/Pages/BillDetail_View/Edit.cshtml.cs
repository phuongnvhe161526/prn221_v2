﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221.Models;

namespace ProjectPRN221.Pages.BillDetail_View
{
    public class EditModel : PageModel
    {
        private readonly ProjectPRN221.Models.ProjectPrn221Context _context;

        public EditModel(ProjectPRN221.Models.ProjectPrn221Context context)
        {
            _context = context;
        }

        [BindProperty]
        public BillDetail BillDetail { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.BillDetails == null)
            {
                return NotFound();
            }

            var billdetail =  await _context.BillDetails.FirstOrDefaultAsync(m => m.BillId == id);
            if (billdetail == null)
            {
                return NotFound();
            }
            BillDetail = billdetail;
           ViewData["BillId"] = new SelectList(_context.Bills, "BillId", "BillId");
           ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "ProductId");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
{
    // Attach the modified BillDetail to the context
    _context.Attach(BillDetail).State = EntityState.Modified;

    try
    {
        // Update the database
        await _context.SaveChangesAsync();

        // Update the quantity in Product
        var product = await _context.Products.FirstOrDefaultAsync(p => p.ProductId == BillDetail.ProductId);
        
        if (product != null)
        {
                    if (int.TryParse(product.QuantityPerUnit, out int currentQuantity))
                    {
                        currentQuantity -= BillDetail.Quantity;

                        // Update QuantityPerUnit back to string
                        product.QuantityPerUnit = currentQuantity.ToString();
                    }
                }

        // Save the changes to the Product
        await _context.SaveChangesAsync();
    }
    catch (DbUpdateConcurrencyException)
    {
        // Handle concurrency exception
        if (!BillDetailExists(BillDetail.BillId))
        {
            return NotFound();
        }
        else
        {
            throw;
        }
    }

    // Redirect to the Index page with the updated BillId
    return RedirectToPage("/BillDetail_View/Index", new { BillId = BillDetail.BillId });
}

        private bool BillDetailExists(int id)
        {
          return (_context.BillDetails?.Any(e => e.BillId == id)).GetValueOrDefault();
        }
    }
}
